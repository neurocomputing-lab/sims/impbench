/*  + Name:
 *  ImpBench/Motion
 *
 *  + Details:
 *  Motion-detection algorithm for movement of animals, based on the description given in
 *  the paper:
 *  R. Puers and P. Wouters, "Adaptable interface Circuits for Flexible Monitoring of
 *  Temperature and Movement", AICSP, Vol. 14, pp. 193--206, 1997.
 *  This is a kernel program. It does (and can) not simulate real-time time aspects of the
 *  actual (hardware) system, such as accelerometer interrupts.
 *  In this algorithm, the degree of activity is actually monitored rather than the exact
 *  value of the amplitude of the activiy signal. That is, the percentage of samples above
 *  a set threshold value in a given monitoring window.
 *  In effect, this motion-detection algorithm is a data-reduction algorithm.
 *  Some algorithm parameters (such as the threshold, the sampling rate etc.)
 *  have been empirically decided upon, since the actual paper does not give specific
 *  information on them.
 *
 *  The program assumes ASCII input files containing a single column of floating-point
 *  numbers, e.g.
 *  -0.00213623	
 *  -0.000305176	
 *  0.00146484	
 *  0.00299072	
 *  0.00445557	
 *  ...
 *  Particular delimitation at the end of each line is not important. It can be:
 *  <CR>, <CR>+<LF>, <TAB>+<CR> or <TAB>+<CR>+<LF> etc..
 *  For the output format see comments in the source code.
 *
 *  To-Do list:
 *  - There is a known issue with binary data that remains to be fixed.
 *
 *  + Syntax:
 *  <exec> <motion-sensor data in filename> <transmission out filename>
 *
 *  + Copyright:
 *  The algorithm concept is the property of the paper authors, as stated above.
 *  This implementation is the property of:
 *  Christos Strydis, CE Lab, TU Delft, 2008. All rights reserved (C).
 */


#include<stdio.h>
#include<stdlib.h>

// Define parameters of the modeled system (and avoid multiplications during program execution)
#define THRESHOLD 0.03
#define SAMPLING_RATE 10 // Hz
#define MONITORING_PERIOD 2 // sec (range: [1..512] sec)
#define IDLE_PERIOD 8 // sec
#define MONITORING_SAMPLES (SAMPLING_RATE * MONITORING_PERIOD) // #samples to be monitored during the monitoring cycle
#define IDLE_SAMPLES (SAMPLING_RATE * IDLE_PERIOD) // #samples to be ignored during the idle cycle

#define byte_round_cast(i) ((unsigned char)((i)+0.5))

int main(int argc, char* argv[]) {

	// General-purpose variables
	char buffer[21];
	double readout;
	unsigned char output;
	unsigned long int active_counter;
	unsigned long int motion_counter;
	unsigned long int idle_counter;

	// I/O-used variables
	FILE *fd_in;
	FILE *fd_out;


	// <exec> <in-file> <out-file>
	if (argc != 3) {
		printf("\nWrong number of arguments!\n");
		return -1;
	}

	// Open input file for reading motion-sensor data
	fd_in = fopen(argv[1],"r");
	if (!fd_in) {
		printf("\nCould not open input file!\n");
		return -1;
	}

	// Open output file for writing (supposedly) transmitted data
	fd_out = fopen(argv[2],"wb");
	if (!fd_out) {
		printf("\nCould not open output file!\n");
		return -1;
	}

	// Print program information
	printf("Motion-detection algorithm");

	// Print program operation settings
	printf("\n\nThreshold: %f \nSensor sampling rate: %d Hz\nMonitoring cycle: %d sec\nIdle cycle: %d sec\nDuty cycle: %.0f%%\n\n", \
               THRESHOLD, SAMPLING_RATE, MONITORING_PERIOD, IDLE_PERIOD, (float)(100*MONITORING_PERIOD/(MONITORING_PERIOD+IDLE_PERIOD)));

	// Motion-detection algorithm
	active_counter = 0;
	motion_counter = 0;
	idle_counter = 0;

	while (!feof(fd_in)) {
		////////////////////////////////////////////////////////////////////////////////////////////////////
		// enter new monitoring mode
		while (active_counter < MONITORING_SAMPLES) {

			// read new sensory data
			fgets(&buffer[0], 21*sizeof(char), fd_in);
			if (feof(fd_in)) {
				printf("\n>Found EOF. Terminating monitoring window.\n"); // $DEBUG$
				break;
			}
			readout = atof(buffer);
			//printf("\n>> New readout = %f.", readout); // $DEBUG$

			// check if new readout is above the threshold value
			if (readout >= THRESHOLD)
				motion_counter ++;
			active_counter ++;
		}

		//printf("\nMonitoring window finished. (%ld/%ld samples above threshold)\n", motion_counter, active_counter); // $DEBUG$


		// write to output file the activity percentage of monitoring window (reduced data)
		//fprintf(fd_out,"%f\n", (float)motion_counter/active_counter);

		// According to the authors, a single byte is written to the output so as to minimize data transmission.
		// For lack of more information, we are conforming to this requirement by outputting only the first
		// decimal-point number of the result,
		// e.g. instead of "0.400000" we output "4" (interpreted as 40%) or 0x4 in hex (as a byte)
		// e.g. instead of "0.557143" we output "6" (interpreted as 60%) or 0x6 in hex (as a byte)
		// e.g. instead of "0.048387" we output "0" (interpreted as 0%) and so on...
		output = byte_round_cast(motion_counter*10.0/active_counter);
		fwrite(&output, sizeof(char), 1, fd_out); // write as binary
		//fprintf(fd_out,"0x%X\n", output); // write as ascii
		

		// reset counters for next monitoring session
		active_counter = 0;
		motion_counter = 0;

		////////////////////////////////////////////////////////////////////////////////////////////////////
		// enter new idle mode
		while ( (idle_counter < IDLE_SAMPLES) && (!feof(fd_in)) ) {
			//fgets(&buffer[0], 21*sizeof(char), fd_in);			// by uncommenting these lines, we allow sensory data to be lost
			if (feof(fd_in)) {                                      // during idle periods (i.e. no monitorinng). This is more realistic
				printf("\n>Found EOF. Terminating idle window.\n");	// but leads to losing a large number of data, especially due to the fact
				break;							                    // that this is a kernel program.
			}								                        //
			idle_counter ++;
		}
		//if (!feof(fd_in))
		//	printf("\nIdle window finished. (%ld lost samples)\n", idle_counter); // $DEBUG$

		// reset counters for next idle session
		idle_counter = 0;
	} // while (!feof(fd_in))


	// Close all opened files
	fclose(fd_in);
	fclose(fd_out);

	return 0;
}
