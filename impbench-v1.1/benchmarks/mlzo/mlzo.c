/*  + Name:
 *  ImpBench/miniLZO
 *
 *  + Details:
 *  This file is based on "testmini.c", a very simple test program for the miniLZO library.
 *
 *  List of changes:
 *  - File name has been changed from "testmini.c" to "mlzo.c" due to uniform naming conventions in
 *    the whole ImpBench suite.
 *  - Decompression has been disabled altogether. We are interested only in compression at this point.
 *    See decompression, in source code below, for what needs to be done.
 *  - Program exit in case of incompressible data (i.e. data expansion) has been disabled.
 *    We are interested in file-saving the actual, expanded data.
 *
 *  To-Do list:
 *  - Source needs to be adjusted to support stand-alone compress/decompress routines
 *    (with separate calculation of input file sizes).
 *
 *  + Syntax:
 *  <exec> <in filename> <out filename>
 *
 *  + Copyright
 *  The initial source is the property of the original authors, as stated below.
 *  This implementation is the property of:
 *  Christos Strydis, CE Lab, TU Delft, 2007. All rights reserved (C).
 * 
 *  The original file is part of the LZO real-time data compression library:
 *  Markus F.X.J. Oberhumer <markus@oberhumer.com>
 *  http://www.oberhumer.com/opensource/lzo/
 */


#include <stdio.h>
#include <stdlib.h>
#include "minilzo.h"


int main(int argc, char *argv[])
{
    int r;

    lzo_bytep in;
    lzo_bytep out;
    lzo_bytep wrkmem;

    lzo_uint in_len;
    lzo_uint out_len;
    lzo_uint new_len;

    FILE *fd1, *fd2, *fd3;


/*change accordigly in case of separate compress/decompress routines (FIXME)*/
    if (argc != 3)
    {
		printf("\nIncorrect number of command-line arguments provided.\n\n");
        return 0;
    }

/*uncomment in case of separate compress/decompress routines (FIXME)*/
/*
    if ((argv[1] != 'c')||(argv[1] != 'd'))
    {
        printf("\nIllegal argument 1 given.\n\n");
        return -1;
    }
*/
    printf("\nLZO real-time data compression library (v%s, %s).\n", lzo_version_string(), lzo_version_date());
    printf("Copyright (C) 1996-2005 Markus Franz Xaver Johannes Oberhumer\nAll Rights Reserved.\n\n");


    /*initialize the LZO library */
    if (lzo_init() != LZO_E_OK)
    {
        printf("internal error - lzo_init() failed !!!\n");
        printf("(this usually indicates a compiler bug - try recompiling\nwithout optimizations, and enable `-DLZO_DEBUG' for diagnostics)\n");
        return 3;
    }


   /*
    * If compression is selected...
    */

    /*open input file and get its size*/
    fd1 = fopen(argv[1],"r");
    if (fd1 == NULL)
    {
       printf("\nCould not open input file (argument 2).\n\n");
       return -2;
    }

    fseek(fd1,0L,SEEK_END);
    in_len = ftell(fd1);
    rewind(fd1);
    
    /*size of output (compressed) file is calculated as follows*/
    out_len = (in_len + in_len / 16 + 64 + 3);

    /*allocate memory space for input and output files*/
    in = (lzo_bytep) malloc(in_len);   /*lzo_malloc*/
    out = (lzo_bytep) malloc(out_len);

    /*allocate memory for workspace*/
    wrkmem = (lzo_bytep) malloc(LZO1X_1_MEM_COMPRESS);
    if (in == NULL || out == NULL || wrkmem == NULL)
    {
        printf("out of memory\n");
        return 3;
    }

    /*open input file to read data*/
    fread(in, 1, in_len, fd1);
    fclose(fd1);


    /*compress from `in' to `out' with LZO1X-1*/
    r = lzo1x_1_compress(in, in_len, out, &out_len, wrkmem);
    if (r == LZO_E_OK)
        printf("compressed %lu bytes into %lu bytes (%%%lu)\n", (unsigned long) in_len, (unsigned long) out_len, (100 - ((unsigned long)out_len * 100/ (unsigned long)in_len)) );
    else
    {
        /* this should NEVER happen */
        printf("internal error - compression failed: %d\n", r);
        return 2;
    }

    /* check for an incompressible block */
    if (out_len >= in_len)
    {
        printf("This block contains incompressible data.\n");
        /*return 0;*/ /*commented out because we want the output file to be created anyway*/
    }

    /*open output file to write compressed data*/
    fd2 = fopen(argv[2],"w");
    if (fd2 == NULL)
    {
       printf("\nCould not open output file (argument 3).\n\n");
       return -3;
    }
    fwrite(out,1,out_len,fd2);
    fclose(fd2);


    /*
     * If decompression is selected...
     * (decompression is currently disabled. Pre-calculating the size of the final uncompressed file
     *  must be resolved first, before making separate encode/decode routines.) (FIXME)
     */

if (0) /*do not execute decompress (FIXME)*/
{
    r = lzo1x_decompress(out, out_len, in, &new_len, NULL);
    if (r == LZO_E_OK && new_len == in_len)
        printf("decompressed %lu bytes back into %lu bytes\n", (unsigned long) out_len, (unsigned long) in_len);
    else
    {
        /* this should NEVER happen */
        printf("internal error - decompression failed: %d\n", r);
        return 1;
    }

    /*open output file to write uncompressed data*/
    fd3 = fopen(argv[3],"w");
    if (fd3 == NULL)
    {
       printf("\nCould not open output file (argument 4).\n\n");
       return -4;
    }
    fwrite(in,1,new_len,fd3);
    fclose(fd3);
} /*do not execute decompress (FIXME)*/

    free(wrkmem);
    free(out);
    free(in);

    printf("\nminiLZO compression/decompression complete.\n");
    return 0;
}
