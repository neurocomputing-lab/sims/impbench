/*  + Name:
 *  ImpBench/CRC32
 *
 *  + Details:
 *  This program is a modified version of the "crc32.c" source. It calculates the CRC32 of a binary
 *  file filled with double-precision numbers. It reads the file a double number at a time, adds in
 *  the CRC accumulated value and copies the read value to a separate binary output file. The final
 *  CRC is finally appended at the end of the file. This copy of data is actually done to avoid
 *  writing the input file. Writing to the output can be easily disabled and the CRC results
 *  returned by the main function.
 *
 *  + Syntax:
 *  <exec> <in filename> <out filename>
 *
 *  + Copyright
 *  The initial source is the property of the original authors, as stated below.
 *  This implementation is the property of:
 *  Christos Strydis, CE Lab, TU Delft, 2008. All rights reserved (C).
 *
 * ******************************************************************************
 *
 * NetBench disclaimer:
 *
 * crc32.c - Routines for crc calculation
 *
 * This file is a part of the NetBench suite
 *
 * This source file is distributed "as is" in the hope that it will be
 * useful. The suite comes with no warranty, and no author or
 * distributor accepts any responsibility for the consequences of its
 * use.
 * 
 * Everyone is granted permission to copy, modify and redistribute
 * this tool set under the following conditions:
 * 
 *    Permission is granted to anyone to make or distribute copies
 *    of this source code, either as received or modified, in any
 *    medium, provided that all copyright notices, permission and
 *    nonwarranty notices are preserved, and that the distributor
 *    grants the recipient permission for further redistribution as
 *    permitted by this document.
 *
 *    Permission is granted to distribute this file in compiled
 *    or executable form under the same conditions that apply for
 *    source code, provied that either:
 *
 *    A. it is accompanied by the corresponding machine-readable
 *       source code,
 *    B. it is accompanied by a written offer, with no time limit,
 *       to give anyone a machine-readable copy of the corresponding
 *       source code in return for reimbursement of the cost of
 *       distribution.  This written offer must permit verbatim
 *       duplication by anyone, or
 *    C. it is distributed by someone who received only the
 *       executable form, and is accompanied by a copy of the
 *       written offer of source code that they received concurrently.
 *
 * In other words, you are welcome to use and share this source file.
 * You are forbidden to forbid anyone else to use, share and improve
 * what you give them. 
 *
 * ******************************************************************************
 *
 * Original disclaimer:
 *
 * crc32.c -- package to compute 32-bit CRC one byte at a time          *
 *                                                                      *
 * Synopsis:                                                            *
 *  gen_crc_table() -- generates a 256-word table containing all CRC    *
 *                     remainders for every possible 8-bit byte.  It    *
 *                     must be executed (once) before any CRC updates.  *
 *                                                                      *
 *  unsigned update_crc(crc_accum, data_blk_ptr, data_blk_size)         *
 *           unsigned crc_accum; char *data_blk_ptr; int data_blk_size; *
 *           Returns the updated value of the CRC accumulator after     *
 *           processing each byte in the addressed block of data.       *
 *                                                                      *
 *  It is assumed that an unsigned long is at least 32 bits wide and    *
 *  that the predefined type char occupies one 8-bit byte of storage.   *
 *                                                                      *
 *  The generator polynomial used for this version of the package is    *
 *  x^32+x^26+x^23+x^22+x^16+x^12+x^11+x^10+x^8+x^7+x^5+x^4+x^2+x^1+x^0 *
 *  as specified in the Autodin Ethernet ADCCP protocol standards.      *
 *  Other degree 32 polynomials may be substituted by re-defining the   *
 *  symbol POLYNOMIAL below.  Lower degree polynomials must first be    *
 *  multiplied by an appropriate power of x.  The representation used   *
 *  is that the coefficient of x^0 is stored in the LSB of the 32-bit   *
 *  word and the coefficient of x^31 is stored in the most significant  *
 *  bit.  The CRC is to be appended to the data most significant byte   *
 *  first.  For those protocols in which bytes are transmitted MSB      *
 *  first and in the same order as they are encountered in the block    *
 *  this convention results in the CRC remainder being transmitted with *
 *  the coefficient of x^31 first and with that of x^0 last (just as    *
 *  would be done by a hardware shift register mechanization).          *
 *                                                                      *
 *  The table lookup technique was adapted from the algorithm described *
 *  by Avram Perez, Byte-wise CRC Calculations, IEEE Micro 3, 40 (1983).*
 */


#include <stdio.h>

#define POLYNOMIAL 0x04c11db7L

static unsigned long crc_table[256];

/* generate the table of CRC remainders for all possible bytes */
void gen_crc_table() {
	register int i, j;  
	register unsigned long crc_accum;

	for (i = 0;  i < 256;  i++) {
		crc_accum = ((unsigned long) i << 24);
		for (j = 0;  j < 8;  j++) {
			if (crc_accum & 0x80000000L)
				crc_accum = (crc_accum << 1) ^ POLYNOMIAL;
			else
				crc_accum = (crc_accum << 1); 
		}
		crc_table[i] = crc_accum; 
	}
	return; 
}

/* update the CRC on the data block one byte at a time */
unsigned long 
update_crc(unsigned long crc_accum, char *data_blk_ptr, int data_blk_size) {
	register int i, j;
	for (j = 0;  j < data_blk_size;  j++) { 
		i = ((int)(crc_accum >> 24) ^ *data_blk_ptr++) & 0xff;
		crc_accum = (crc_accum << 8) ^ crc_table[i]; 
	}
	return crc_accum; 
}



int main (int argc, char **argv) {
	unsigned long crc_accum = 0xFFFFFFFFL; // http://cell.onecall.net/cell-relay/publications/software/CRC/32bitCRC.html
	int numpackets = 0;
	unsigned char packet[8]; // use sizeof(double) as default data-packet size
 	FILE * fd_in, * fd_out;
  
	if (argc != 3) {
		printf ("\nUsage: <exec> <in filename> <out filename>\n");
		return -1;
	}

	// Open input file for reading
	fd_in = fopen(argv[1],"r");
	if (!fd_in) {
		printf("\nCould not open %s file!\n", argv[1]);
		return -1;
	}

	// Open file for writing
	fd_out = fopen(argv[2],"w");
	if (!fd_out) {
		printf("\nCould not open %s file!\n", argv[2]);
		return -1;
	}

	// Program information
	printf ("\n32-degree polynomial Cyclic-Redundancy-Check (CRC32).\n");

	// Generate CRC table
	printf ("\nGenerate CRC32 table based on 0x%.8lX polynomial.\n", POLYNOMIAL);
	gen_crc_table();

	while (1) {
		//int i; // $DEBUG$ (1 of 2)
		fread(packet, sizeof(double), 1, fd_in); // read next packet from file
		if (feof(fd_in))
			break;

		crc_accum += update_crc (crc_accum, packet, 8); // update CRC with new value read
		numpackets ++; // increase the number of packets processed by one

		//printf("\npacket: ");			// $DEBUG$ (2 of 2)
		//for (i=0;i<8;i++)			//
		//	printf("%.2x ", packet[i]);	//
		//printf("- numpackets: %d", numpackets); //

		fwrite(packet, sizeof(double), 1, fd_out); // copy packet to output file (bin)
	}
	fwrite(&crc_accum, sizeof(unsigned long), 1, fd_out); // finally, append CRC number to the output file (bin)

	printf ("\nCRC completed: 0x%lX (for %d packets).\n", crc_accum, numpackets);

	fclose(fd_in);
	fclose(fd_out);

	return 0;
}
