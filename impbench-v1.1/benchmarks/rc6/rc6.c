/*  + Name:
 *  ImpBench/RC6
 *
 *  + Details:
 *  All needed sources have been merged in a single file. A main function has been
 *  written, data are fed in in blocks, block size and encryption key are
 *  command-line specifiable.
 *  The decryption part is commented out to isolate encryption functionality.
 *  Uncomment the call to "RC6_32_decrypt()" in the main function to restore it.
 *
 *  To-Do list:
 *  - Source needs to be adjusted to support stand-alone encrypt/decrypt routines
 *    (with separate calculation of input file sizes).
 *
 *  + Syntax:
 *  <exec> <in filename> <out filename> <block-size> <key>
 *
 *  + Copyright:
 *  The initial source is the property of the original authors, as stated below.
 *  This implementation is the property of:
 *  Christos Strydis and Di Zhu, CE Lab, TU Delft, 2008. All rights reserved (C).
 *
 *  Original RC6 implementation using 32-bit word size
 *  written by Yee Wei Law.
 */


#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#undef c2l
#define c2l(c,l)	(l =((unsigned long)(*((c)++))), \
			 l|=((unsigned long)(*((c)++)))<< 8L, \
			 l|=((unsigned long)(*((c)++)))<<16L, \
			 l|=((unsigned long)(*((c)++)))<<24L)

/* NOTE - c is not incremented as per c2l */
#undef c2ln
#define c2ln(c,l1,l2,n)	{ \
			c+=n; \
			l1=l2=0; \
			switch (n) { \
			case 8: l2 =((unsigned long)(*(--(c))))<<24L; \
			case 7: l2|=((unsigned long)(*(--(c))))<<16L; \
			case 6: l2|=((unsigned long)(*(--(c))))<< 8L; \
			case 5: l2|=((unsigned long)(*(--(c))));     \
			case 4: l1 =((unsigned long)(*(--(c))))<<24L; \
			case 3: l1|=((unsigned long)(*(--(c))))<<16L; \
			case 2: l1|=((unsigned long)(*(--(c))))<< 8L; \
			case 1: l1|=((unsigned long)(*(--(c))));     \
				} \
			}

#undef l2c
#define l2c(l,c)	(*((c)++)=(unsigned char)(((l)     )&0xff), \
			 *((c)++)=(unsigned char)(((l)>> 8L)&0xff), \
			 *((c)++)=(unsigned char)(((l)>>16L)&0xff), \
			 *((c)++)=(unsigned char)(((l)>>24L)&0xff))

/* NOTE - c is not incremented as per l2c */
#undef l2cn
#define l2cn(l1,l2,c,n)	{ \
			c+=n; \
			switch (n) { \
			case 8: *(--(c))=(unsigned char)(((l2)>>24L)&0xff); \
			case 7: *(--(c))=(unsigned char)(((l2)>>16L)&0xff); \
			case 6: *(--(c))=(unsigned char)(((l2)>> 8L)&0xff); \
			case 5: *(--(c))=(unsigned char)(((l2)     )&0xff); \
			case 4: *(--(c))=(unsigned char)(((l1)>>24L)&0xff); \
			case 3: *(--(c))=(unsigned char)(((l1)>>16L)&0xff); \
			case 2: *(--(c))=(unsigned char)(((l1)>> 8L)&0xff); \
			case 1: *(--(c))=(unsigned char)(((l1)     )&0xff); \
				} \
			}

/* NOTE - c is not incremented as per n2l */
#define n2ln(c,l1,l2,n)	{ \
			c+=n; \
			l1=l2=0; \
			switch (n) { \
			case 8: l2 =((unsigned long)(*(--(c))))    ; \
			case 7: l2|=((unsigned long)(*(--(c))))<< 8; \
			case 6: l2|=((unsigned long)(*(--(c))))<<16; \
			case 5: l2|=((unsigned long)(*(--(c))))<<24; \
			case 4: l1 =((unsigned long)(*(--(c))))    ; \
			case 3: l1|=((unsigned long)(*(--(c))))<< 8; \
			case 2: l1|=((unsigned long)(*(--(c))))<<16; \
			case 1: l1|=((unsigned long)(*(--(c))))<<24; \
				} \
			}

/* NOTE - c is not incremented as per l2n */
#define l2nn(l1,l2,c,n)	{ \
			c+=n; \
			switch (n) { \
			case 8: *(--(c))=(unsigned char)(((l2)    )&0xff); \
			case 7: *(--(c))=(unsigned char)(((l2)>> 8)&0xff); \
			case 6: *(--(c))=(unsigned char)(((l2)>>16)&0xff); \
			case 5: *(--(c))=(unsigned char)(((l2)>>24)&0xff); \
			case 4: *(--(c))=(unsigned char)(((l1)    )&0xff); \
			case 3: *(--(c))=(unsigned char)(((l1)>> 8)&0xff); \
			case 2: *(--(c))=(unsigned char)(((l1)>>16)&0xff); \
			case 1: *(--(c))=(unsigned char)(((l1)>>24)&0xff); \
				} \
			}

#undef n2l
#define n2l(c,l)        (l =((unsigned long)(*((c)++)))<<24L, \
                         l|=((unsigned long)(*((c)++)))<<16L, \
                         l|=((unsigned long)(*((c)++)))<< 8L, \
                         l|=((unsigned long)(*((c)++))))

#undef l2n
#define l2n(l,c)        (*((c)++)=(unsigned char)(((l)>>24L)&0xff), \
                         *((c)++)=(unsigned char)(((l)>>16L)&0xff), \
                         *((c)++)=(unsigned char)(((l)>> 8L)&0xff), \
                         *((c)++)=(unsigned char)(((l)     )&0xff))

#ifndef ROTATE_l16
#define ROTATE_l16(a,n)     (((a)<<(n&0xf))|(((a)&0xffff)>>(16-(n&0xf))))
#endif
#ifndef ROTATE_r16
#define ROTATE_r16(a,n)     (((a)<<(16-(n&0xf)))|(((a)&0xffff)>>(n&0xf)))
#endif
#ifndef ROTATE_l32
#define ROTATE_l32(a,n)     (((a)<<(n&0x1f))|(((a)&0xffffffff)>>(32-(n&0x1f))))
#endif
#ifndef ROTATE_r32
#define ROTATE_r32(a,n)     (((a)<<(32-(n&0x1f)))|(((a)&0xffffffff)>>(n&0x1f)))
#endif

#define RC6_16_MASK	0xffffL
#define RC6_32_MASK	0xffffffffL

#define RC6_16_P	0xB7E1
#define RC6_16_Q	0x9E37
#define RC6_32_P	0xB7E15163L
#define RC6_32_Q	0x9E3779B9L


typedef unsigned int u32;

#define RC6_32_ROUNDS	20  //specification-compliant

typedef struct rc6_32_key_st {
    unsigned long S[(RC6_32_ROUNDS<<1)+4];
    int rounds;
} RC6_32_KEY;

// originally used for fully unrolled speed-optimised version
// but resulting code size is over 10 KB -- too large
// now deprecated
#define ENCRYPT_ROUND(A, B, C, D, i)	\
   t = ROTATE_l32((((B*B)<<1)+B), LOGW);\
   u = ROTATE_l32((((D*D)<<1)+D), LOGW);\
   A = ROTATE_l32(A^t, u) + S[i];\
   C = ROTATE_l32(C^u, t) + S[i+1];

#define DECRYPT_ROUND(A, B, C, D, i)	\
	u = ROTATE_l32((((D*D)<<1)+D), LOGW);\
	t = ROTATE_l32((((B*B)<<1)+B), LOGW);\
	C = ROTATE_r32((C - S[i]), t)^u;\
	A = ROTATE_r32((A - S[i-1]), u)^t;


#define LOGW 5

/**
 * Note: This version is better than the previous one, such that it even
 * improves encryption, but I have to redo the benchmark.
 */
void RC6_32_set_key(const unsigned char *userKey, int bits, RC6_32_KEY *key) 
{
    int slen = (RC6_32_ROUNDS<<1)+4;    // length of expanded key S
    int c = bits>>LOGW;                 // number of words in userKey
    int v;				// number of key setup (not encryption) rounds
    int i, j, s;                        // counters
    u32 L[8];          			// max 256 bits = 8 32-bit words
    u32 *S = key->S, A=0, B=0;    

    key->rounds = RC6_32_ROUNDS;

    // transfer userKey into L
    for (i = j = 0; i < c; i++) {        
        L[i]  = (u32)userKey[j++];
        L[i] ^= (u32)userKey[j++]<<8;
        L[i] ^= (u32)userKey[j++]<<16;
        L[i] ^= (u32)userKey[j++]<<24;
    }

	// init S
    S[0] = RC6_32_P;
    for (i = 1; i < slen; i++) {
        S[i] = S[i-1] + RC6_32_Q;
    }
            
    // do the mixing
	v = (c > slen ? c : slen);
	v = (v<<1)+v;	// multiply by 3
    for (s = 1, i = j = 0; s <= v; s++) {     
        A = S[i] = ROTATE_l32((S[i]+A+B), 3);
        B = L[j] = ROTATE_l32((L[j]+A+B), (A+B));
        if (++i == slen) i = 0;
        if (++j == c) j = 0;
    }
}

/* Works on 4 words at a time. */
void RC6_32_encrypt(const unsigned char *in, unsigned char *out, const RC6_32_KEY *key) 
{    
    u32 A=0, B=0, C=0, D=0, i=0, t=0, u=0;
    
    // bytes to words 
    // the first byte of plaintext is placed in the LSB(yte) of A
    // the last byte of plaintext is placed in the MSB(yte) of D
    A = (u32)in[3]<<24 ^ (u32)in[2]<<16 ^ (u32)in[1]<<8 ^ (u32)in[0];
    B = (u32)in[7]<<24 ^ (u32)in[6]<<16 ^ (u32)in[5]<<8 ^ (u32)in[4];
    C = (u32)in[11]<<24 ^ (u32)in[10]<<16 ^ (u32)in[9]<<8 ^ (u32)in[8];
    D = (u32)in[15]<<24 ^ (u32)in[14]<<16 ^ (u32)in[13]<<8 ^ (u32)in[12];
    
    //printf("A=%x,B=%x,C=%x,D=%x\n",A,B,C,D); // [debug]
    
    // encrypt
    B += key->S[0];
    D += key->S[1];
	// we don't unroll this, because the resulting code size is over 
	// 10 KB -- too large
    for (i = 1; i <= key->rounds; i++) {
        t = ROTATE_l32((((B*B)<<1)+B), LOGW);
        u = ROTATE_l32((((D*D)<<1)+D), LOGW);
        A = ROTATE_l32((A^t), u) + key->S[i<<1];
        C = ROTATE_l32((C^u), t) + key->S[(i<<1)+1];
        
        t = A; A = B; B = C; C = D; D = t; // permute
    }
    A += key->S[(key->rounds<<1)+2];
    C += key->S[(key->rounds<<1)+3];

    // words to bytes
    // the first byte of plaintext is placed in the LSB(yte) of A
    // the last byte of plaintext is placed in the MSB(yte) of D    
    out[3] = A>>24; out[2] = A>>16; out[1] = A>>8; out[0] = A&0xff;
    out[7] = B>>24; out[6] = B>>16; out[5] = B>>8; out[4] = B&0xff;
    out[11] = C>>24; out[10] = C>>16; out[9] = C>>8; out[8] = C&0xff;
    out[15] = D>>24; out[14] = D>>16; out[13] = D>>8; out[12] = D&0xff;
}

/* Works on 4 words at a time. */
void RC6_32_decrypt(const unsigned char *in, unsigned char *out, const RC6_32_KEY *key) 
{
    u32 A=0, B=0, C=0, D=0, i=0, t=0, u=0;  
    
    // bytes to words 
    // the first byte of plaintext is placed in the LSB(yte) of A
    // the last byte of plaintext is placed in the MSB(yte) of D
    A = (u32)in[3]<<24 ^ (u32)in[2]<<16 ^ (u32)in[1]<<8 ^ (u32)in[0];
    B = (u32)in[7]<<24 ^ (u32)in[6]<<16 ^ (u32)in[5]<<8 ^ (u32)in[4];
    C = (u32)in[11]<<24 ^ (u32)in[10]<<16 ^ (u32)in[9]<<8 ^ (u32)in[8];
    D = (u32)in[15]<<24 ^ (u32)in[14]<<16 ^ (u32)in[13]<<8 ^ (u32)in[12];
    
    // decrypt
    C -= key->S[(key->rounds<<1)+3];
    A -= key->S[(key->rounds<<1)+2];
	// we don't unroll this, because the resulting code size is over 
	// 10 KB -- too large
    for (i = key->rounds; i >= 1; i--) {
        t = D; D = C; C = B; B = A; A = t; // permute

        u = ROTATE_l32((((D*D)<<1)+D), LOGW);
        t = ROTATE_l32((((B*B)<<1)+B), LOGW);
        C = ROTATE_r32((C - key->S[(i<<1)+1]), t)^u;
        A = ROTATE_r32((A - key->S[i<<1]), u)^t;
    }
    D -= key->S[1];
    B -= key->S[0];

    // words to bytes
    // the first byte of plaintext is placed in the LSB(yte) of A
    // the last byte of plaintext is placed in the MSB(yte) of D    
    out[3] = A>>24; out[2] = A>>16; out[1] = A>>8; out[0] = A&0xff;
    out[7] = B>>24; out[6] = B>>16; out[5] = B>>8; out[4] = B&0xff;
    out[11] = C>>24; out[10] = C>>16; out[9] = C>>8; out[8] = C&0xff;
    out[15] = D>>24; out[14] = D>>16; out[13] = D>>8; out[12] = D&0xff;
}


int main(int argc, char* argv[]) {
	unsigned char * key; // key size: 128, 192 or 256 bits
	RC6_32_KEY  rc6_key;
	unsigned char data[16]; // block size: 128 bits
	unsigned char encr_data[16];

	// I/O-used variables
	FILE *fd1, *fd2;
	unsigned long dsize, bsize;
	long fsize, tempsize;
	int i;

	// <exec> <in-file> <out-file> <block-size> <key>
	if (argc != 5) {
		printf("\nWrong number of arguments!\n");
		return -1;
	}

	// open input file for reading
	fd1 = fopen(argv[1],"r");
	if (!fd1) {
		printf("\nCould not open input file!\n");
		return -1;
	}

	// find the size of the input file
	fsize = ftell(fd1);
	fseek(fd1,0L,SEEK_END);
	fsize = ftell(fd1) - fsize; // plaintext size in bytes
	rewind(fd1);

	// open output file for writing
	fd2 = fopen(argv[2],"w");
	if (!fd2) {
		printf("\nCould not open output file!\n");
		return -1;
	}

	// get block size from command line
	bsize = dsize = atoi(argv[3]); // block size is static (16 Bytes)

	// get key from command line
	i = strlen(argv[4]);
	if ((i != 16)&&(i != 24)&&(i != 32)) {
		printf("\nWrong key size! Exiting...\n");
		return -1;
	}
	key = malloc(i*sizeof(char));
	memcpy(key,argv[4], i);
	RC6_32_set_key(key, (i<<3), &rc6_key); // we use (i<<3) since i is in Bytes and we need Bits here

	//for(i=0;i<8;i++)
	//	printf("%x\n",key[i]);

	printf("RC6 encryption algorithm\n(plaintext size: %lu B, block size: %u B, key: \"%s\")\n",fsize,16,key);

	// start iterations on plaintext
	while (fsize >= 0) {
		tempsize = fsize;
		fsize -= dsize;
		if (fsize < 0) {
			for (i=0; i<bsize; i++)
				data[i] = 0; // padding
			dsize = tempsize;
		}

		fread(&data[0], dsize, 1, fd1);

		// encrypt
		RC6_32_encrypt(data, encr_data, &rc6_key);

		// decrypt
		//RC6_32_decrypt(encr_data, data, &rc6_key); // if you want to enable decryption, uncomment this line

		fwrite(&data[0], bsize, 1, fd2); //decrypt quantized to block size (bsize instead of dsize)
		//printf("\nfsize: %ld, tempsize: %ld, dsize: %lu",fsize, tempsize, dsize); //DEBUG
	} //while

	free(key);
	fclose(fd1);
	fclose(fd2);

	return 0;
}
