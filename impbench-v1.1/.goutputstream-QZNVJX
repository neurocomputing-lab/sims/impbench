ImpBench Suite, Release 1.1
Christos Strydis, CE Laboratory, TU Delft, 2008 (C)


QUICKSTART:

	All benchmarks (under the 'benchmarks' directory) come with their
        own Makefile files for manually building their binaries.
        Sanity checking of the whole ImpBench package as well as automatic
        setup and testing (i.e. demo) of different benchmarks can be done
        by executing the 'init.sh' Bash script, found under the top-level
        directory. For more details, read the script comments. You can
	run it as follows:

	> bash ./init.sh -da

        For more available commands, run:

	> bash ./init.sh -he


OVERVIEW:

	This README file is for the ImpBench suite release 1.0. This release
	is available from:

	http://ce.et.tudelft.nl/SiMS/


HOW TO BUILD THE BINARIES:

	Each benchmark subdirectory, under "benchmarks", contains its own
	Makefile file. In case all binaries need to be automatically built,
        run the 'init.sh' Bash script, found in the top-level directory.
        Thus, the command to automatically build all sources is:

	> bash ./init.sh -bb

	Tool dependences:
	+ GNU Make
	+ Bash shell
	+ Perl
	+ GNU GCC


BENCHMARKS:

	There are currently 8 benchmarks in ImpBench: mlzo, fin, misty1, rc6,
	checksum, crc32, motion and dmu. More information about the benchmarks
	and an involved discussion on their characteristics can be found in
	the conference paper:

	ImpBench: A novel benchmark suite for biomedical, microelectronic
		  implants
	(http://ce.et.tudelft.nl/publicationfiles/1538_555_samos.pdf).

	For known and/or pending unresolved issues (if any) in the benchmark
	sources, please find a complete list at the header comments of each
	benchmark's top source file.


INPUT DATASETS: 
	
	ImpBench has been evaluated on physiological-data-related inputs ripped
	from the "Biopac Student Lab 3.7" (BSL) software. The specific datasets
	can be found under the "files_input" directory. They comprise 7
	different data types: AEP, BP, ECG, EEG, EMG, RC, PF. Each data type
	comes in two sizes (1 KB and 10 KB), in two encoding formats (8-bit
	ASCII and double-precision binary). More details on the input datasets
	can be found in "ImpBench Suite v1.0 � Release Notes.pdf", Appx. D,
	under the "documentation" directory.


COMPATIBLE PLATFORMS:

	ImpBench has been developed and tested on a Intel Pentium 4 running
	"Fedora Core 5" Linux (2.6.20-1.2320.fc5smp) 2.2.17 with gnu/gcc-4.1.1
	and simplescalar/arm cross-gcc-2.95.2; but it should be able to build
	and run in almost all systems.


FURTHER INFORMATION:

	For a detailed discussion of the ImpBench-suite contents, copyrights and
	structure, refer to the "impbench-v1.0_release-notes.pdf"
	document, under the "documentation" directory.


CONTACT:

	Christos Strydis

	Computer Engineering Laboratory
	Electrical Engineering Department
	Delft University of Technology
 
	Mekelweg 4 (15th floor)
	2628 CD Delft
	The Netherlands

	E-mail: C.Strydis@tudelft.nl
